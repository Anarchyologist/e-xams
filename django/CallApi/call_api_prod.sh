#!/bin/bash
NAME="CallApi"
DJANGODIR=/webappz/pydev/sites/e-xams/django/CallApi
SOCKFILE=/webappz/pydev/run/anarchyologist.sock

USER=pydev
GROUP=pydev
NUM_WORKERS=9
TIMEOUT=240
MAX_REQUESTS=0
DJANGO_SETTINGS_MODULE=CallApi.settings
DJANGO_WSGI_MODULE=CallApi.wsgi

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source /webappz/pydev/venvs/exam/bin/activate

export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn’t exist
RUNDIR=$(dirname $SOCKFILE)

# Remove previous socket if any
rm -f $SOCKFILE

# Start your Django Unicorn

# Programs meant to be run under supervisor should not daemonize themselves (do not use –daemon)
exec /webappz/pydev/venvs/exam/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
--name $NAME \
--workers $NUM_WORKERS \
--max-requests $MAX_REQUESTS \
--timeout $TIMEOUT \
--user $USER  \
-b unix:$SOCKFILE \
#--reload \
--log-level error \
--log-file -

