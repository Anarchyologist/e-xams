from django.conf.urls import url, include
from django.conf.urls.static import static
from django.conf import settings
from rest_framework import routers
from rest_framework.routers import DefaultRouter
from .viewsets import AmViewSet, ExamViewSet, ExamInstanceViewSet
from django.urls import path


router = routers.DefaultRouter()
router.register(r'instance',ExamInstanceViewSet, basename='ExamInstance')
router.register(r'exam', ExamViewSet, basename='Exam')
router.register(r'', AmViewSet)

urlpatterns=[
    path(r'',include(router.urls)),
] 

