import logging
# Local Library
from rest_framework import viewsets
from TestersApp.models import AM
from TestersApp.serializers import AMSerializer
from threading import local
from rest_framework.response import Response
from TestersApp.viewsets import AmViewSet
from rest_framework.response import Response
from rest_framework.exceptions import APIException


class Correlation():

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.META['examtoken'] = request.headers.get('Authorization')
       # try:
        #    usrtoken = AM.objects.get(examtoken=request.META['examtoken'])
        response = self.get_response(request)
        return response
      #  except AM.DoesNotExist:
      #      raise APIException('Doesnt Exist')

#class CorrelationFilter(logging.Filter):
#
#    def filter(self, record):
#        if not hasattr(record, 'UserToken'):
#            record.UserToken = ""
#        if hasattr(_locals, 'UserToken'):
#            record.UserToken = _locals.UserToken
#        return True
