from django.shortcuts import render
from django.db.models import Count
import requests
from rest_framework import viewsets
from TestersApp.models import ExamInstance, AM, Exam
from TestersApp.serializers import AMSerializer, ExamSerializer, ExamInstanceSerializer
from django.http import FileResponse, HttpResponse
from wsgiref.util import FileWrapper
from django.contrib.auth import authenticate, login
from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny , IsAdminUser , IsAuthenticated
from rest_framework.response import Response
from rest_framework.exceptions import APIException
import datetime, time

import uuid, os, subprocess, stat, signal, tempfile
from subprocess import check_output
import multiprocessing as mp
from rest_framework_jwt.settings import api_settings
from django.utils import timezone
from threading import Timer
import json
import zipfile
from django.core.mail import send_mail


class AmViewSet(viewsets.ModelViewSet):

    queryset = AM.objects.all() 
    serializer_class = AMSerializer

    @action(detail=False , methods=['post'] , permission_classes=[AllowAny])
    def login(self , request):
        try:
            response1 = request.data.get('AM')
            response2 = request.data.get('unique_id')
            try:
                user = AM.objects.get(TestersAM=request.data['AM'] , unique_id=request.data['unique_id'])
                if user.examtoken=="":
                    user.examtoken = uuid.uuid4() 
                    user.save()
            except AM.DoesNotExist:
                raise APIException('Κάποιο απο τα στοιχεία είναι λανθασμένα')
        except KeyError:
            raise APIException('Κάτι ξέχασες!')

        return Response( { user.examtoken } )


    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def check(self , request):
        try:
            AM.objects.get(examtoken=request.data['token'])
        except AM.DoesNotExist:
            raise APIException('Προσπάθησε πάλι')

        return Response( True )


    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def logout(self, request):
        try:
            AM.objects.filter(examtoken=request.data['temptoken1']).update(examtoken="")
        except AM.DoesNotExist:
            raise APIException('Κάτι πήγε στραβά') 

        return Response( "Its Done" )


    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def gettestam(self , request):
        try:
            am = AM.objects.get(examtoken=request.data['temptoken'])
        except AM.DoesNotExist:
            raise APIException('Kάτι πήγε στραβά!')
        getexam = ExamInstance.objects.filter(student=am.pk).values_list('exam_type', flat=True)
        res = []
        show = []
        for ex in getexam:
            if not ex==None:
                add = Exam.objects.get(id=ex)
                tmp = ExamInstance.objects.get(exam_type=add.pk, student=am.pk)
                if tmp.timer:
                    aa = tmp.timer < timezone.localtime()
                    if aa:
                        tmp.finished = True
                        tmp.save()
                get1 = ExamInstanceSerializer([tmp], many=True)
                get = ExamSerializer([add], many=True)
                res = get.data + res
                show = get1.data + show
        #res = res + [AMSerializer([am])]
        return Response( { 'exam': res, 'am': am.TestersAM, 'inst': show } )


    @action(detail=False, methods=['get'], permission_classes=[AllowAny])
    def sendmail(self , request):
        try:
            mai = AM.objects.all()
            for am in mai:
                #print(am.email)
                send_mail(
                        'Your E-xams Password',
                        'Ο κωδικός πρόσβασης για να εξεταστείς στο menaixmos.com είναι : ' + am.unique_id,
                        'mailforexams1@gmail.com',
                        [am.email],
                        )
        except AM.DoesNotExist:
            raise APIException('Δεν υπάρχουν εξεταζόμενοι')

        return Response('Done')

    @action(detail=False, methods=['get'], permission_classes=[AllowAny])
    def getCode(self , request):
        ams = AM.objects.all()

        my_zip = zipfile.ZipFile('TestersCodes.zip', 'a', zipfile.ZIP_DEFLATED)

        filelist = [ f for f in os.listdir(settings.MEDIA_ROOT+'/TestersCodes/')]
        for f in filelist:
            ex = [ k for k in os.listdir(os.path.join(settings.MEDIA_ROOT,'TestersCodes/',f)) ]
            for k in ex:
                a = os.path.join(settings.MEDIA_ROOT,'TestersCodes/', f+'/', k)
                if os.path.exists(a):
                   with open(a, 'rb') as fi:
                       my_zip.write(fi.name, arcname = f+"\\"+k)

        my_zip.close()
        os.rename('TestersCodes.zip', settings.MEDIA_ROOT+'/'+'TestersCodes.zip')

        return Response('Done')

    @action(detail=False, methods=['get'], permission_classes=[AllowAny])
    def getResu(self , request):
        try:
            inst = ExamInstance.objects.all()
            if os.path.exists(os.path.join(settings.MEDIA_ROOT,"Results.txt")):
               os.remove(settings.MEDIA_ROOT+"/Results.txt")
            temp = open(settings.MEDIA_ROOT+"/Results.txt", "a+")
            i = 0
            for am in inst:
                try:
                    aaa = am.exam_type.pk
                    try:
                        exam = Exam.objects.get(pk=am.exam_type.pk)
                        stude = AM.objects.get(pk=am.student.pk)
                        if i!=am.exam_type.pk:
                            i = am.exam_type.pk
                            temp.write("\n")
                        if am.finished==True and am.passed==True:
                            temp.write(stude.TestersAM+"   "+exam.exam_name+"     Πέρασε\n")
                        elif am.finished==True and am.passed==False:
                            temp.write(stude.TestersAM+"   "+exam.exam_name+"     Δεν Πέρασε\n")
                        elif am.finished==False:
                            temp.write(stude.TestersAM+"   "+exam.exam_name+"     DNF\n")
                    except Exam.DoesNotExists:
                        raise APIException('Δεν υπάρχουν εξετάσης')
                except AttributeError:
                    continue


        except ExamInstance.DoesNotExist:
            raise APIException('Δεν υπάρχουν εξετάσης')

        return Response('Done')



#-----------------------------------------------------------------------------------------------------------------------

class ExamViewSet(viewsets.ModelViewSet):

    queryset = Exam.objects.all()
    serializer_class = ExamSerializer


    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def inserttest(self , request):
        try:
            addtest = Exam.objects.get(exam_name=request.data['examid'])
            for i in range(len(request.data['ams'])):
                k = AM.objects.get(TestersAM=request.data['ams'][i]['TestersAM'])
                inst = ExamInstance.objects.get(exam_type=addtest.pk, student=k.pk)
                if (request.data['type']=='exam-0' or request.data['type']=='exam-3'):
                    if not os.path.exists(os.path.join(settings.MEDIA_ROOT, 'TestersCodes/'+k.TestersAM+'/',(k.TestersAM+"_"+addtest.exam_name+".cpp"))):
                        for teo in request.data['input']:
                            if teo==None:
                                continue
                            if 'codein' in teo[0]:
                                #print(teo)
                                inst.AmCode.save(k.TestersAM+"_"+addtest.exam_name+".cpp", ContentFile(teo[0]['codein']))
                k.email = request.data['email'][i]
                k.save()

            if (request.data['type']=='exam-2'):
                if not request.data['input'][0]==None:
                    addtest.io_pairs = request.data['input']
                else:
                    addtest.io_pairs = request.data['input'][1]
            if (request.data['type']=='exam-0' or request.data['type']=='exam-3'):
                for ex in request.data['input']:
                    if ex==None:
                        continue
                    if 'output' in ex[0]:
                        addtest.io_pairs = ex[0]
            if (request.data['type']=='exam-1'):
                for ex in request.data['input']:
                    if ex==None:
                        continue
                #print(ex)
                    if 'data' in ex[0]:
                        addtest.the_input.save(addtest.exam_name+".txt", ContentFile(ex[0]['data']))
                    if 'output' in ex[0]:
                        addtest.io_pairs = ex[0]
                    if 'codein' in ex[0]:
                        addtest.test_codein.save(addtest.exam_name+".cpp", ContentFile(ex[0]['codein']))
            addtest.the_tests = request.data['thetest']
            addtest.exam_type = request.data['type']
            addtest.date = timezone.localtime()
            addtest.save()
        except Exam.DoesNotExist:
            raise APIException('Κάτι πήγε στραβά προσπάθησε πάλι')
        except AM.DoesNotExist:
            raise APIException('Κάτι πήγε στραβά προσπάθησε πάλι')

        return Response( 'Done' )


    @action(detail=False, methods=['get'], permission_classes=[AllowAny])
    def gettest(self , request):
        try:
            fo = Exam.objects.all()
            ids = fo.values_list('pk', flat=True)
            countam = []
            for theid in ids:
                add = Exam.objects.get(pk=theid)
                isfinish = ExamInstance.objects.filter(exam_type=add.pk)
                for fi in isfinish:
                    tmp = True
                    if (fi.finished==True and tmp==True):
                        add.finish=True
                        add.save()
                    else:
                        add.finish=False
                        add.save()
                        tmp = False
                countam.append(isfinish.count())
            get = ExamSerializer(fo, many=True)
        except Exam.DoesNotExist:
            raise APIException('Δεν υπάρχουν εξετάσεις')

        return Response( { 'test': get.data, 'count': countam } )

    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def showsel(self, request):
        try:
            exam = Exam.objects.get(exam_name=request.data['name'])
            exam.showexam = True
            exam.save()
        except Exam.DoesNotExist:
            raise APIException('Δεν υπάρχει η εξέταση %s' %request.data['name'])

        return Response('Done')

    @action(detail=False, methods=['get'], permission_classes=[AllowAny])
    def deleteall(self , request):
        delet = AM.objects.all().delete()
        delet = Exam.objects.all().delete()
        try:
            filelist = [ f for f in os.listdir(settings.MEDIA_ROOT+'/TestersCodes/') ]
            for f in filelist:
                tester = [ k for k in os.listdir(os.path.join(settings.MEDIA_ROOT,'TestersCodes/',f)) ]
                for k in tester:
                    os.remove(os.path.join(settings.MEDIA_ROOT,'TestersCodes/' ,f+'/', k))
                os.rmdir(os.path.join(settings.MEDIA_ROOT, 'TestersCodes/', f))

            filelist1 = [ f for f in os.listdir(settings.MEDIA_ROOT+'/Tests/')]
            for f in filelist1:
                ex = [ k for k in os.listdir(os.path.join(settings.MEDIA_ROOT,'Tests/',f)) ]
                for k in ex:
                    os.remove(os.path.join(settings.MEDIA_ROOT,'Tests/' ,f+'/', k))
                os.rmdir(os.path.join(settings.MEDIA_ROOT, 'Tests/', f))

            filelist2 = [ f for f in os.listdir(settings.MEDIA_ROOT+'/Inputs/')]
            for f in filelist2:
                os.remove(os.path.join(settings.MEDIA_ROOT, 'Inputs/', f))
            filelist3 = [ f for f in os.listdir(settings.MEDIA_ROOT+'/TestsCodein/')]
            for f in filelist3:
                os.remove(os.path.join(settings.MEDIA_ROOT, 'TestsCodein/', f))
        except FileNotFoundError:
            delet = AM.objects.all().delete()
            delet = Exam.objects.all().delete()
        return Response("success")

    @action(detail=False, methods=['get'], permission_classes=[AllowAny])
    def deleteExam(self , request):
        delet = Exam.objects.all().delete()
        try:
            filelist1 = [ f for f in os.listdir(settings.MEDIA_ROOT+'/Tests/')]
            for f in filelist1:
                ex = [ k for k in os.listdir(os.path.join(settings.MEDIA_ROOT,'Tests/',f)) ]
                for k in ex:
                    os.remove(os.path.join(settings.MEDIA_ROOT,'Tests/' ,f+'/', k))
                os.rmdir(os.path.join(settings.MEDIA_ROOT, 'Tests/', f))

            filelist2 = [ f for f in os.listdir(settings.MEDIA_ROOT+'/Inputs/')]
            for f in filelist2:
                os.remove(os.path.join(settings.MEDIA_ROOT, 'Inputs/', f))
            filelist3 = [ f for f in os.listdir(settings.MEDIA_ROOT+'/TestsCodein/')]
            for f in filelist3:
                os.remove(os.path.join(settings.MEDIA_ROOT, 'TestsCodein/', f))
        except FileNotFoundError:
            delet = Exam.objects.all().delete()
        return Response("success")


    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def deletesel(self , request):
        try:
            te = Exam.objects.get(exam_name=request.data['name'])
            delet = ExamInstance.objects.filter(exam_type=te.pk)
            os.remove(os.path.join(settings.MEDIA_ROOT, 'Inputs/',request.data['name']+".txt"))
            os.remove(os.path.join(settings.MEDIA_ROOT, 'TestsCodein/',request.data['name']+".cpp"))
            for i in range(len(delet)):
                k = AM.objects.get(pk=delet[i].student)
                os.remove(os.path.join(settings.MEDIA_ROOT, 'Tests/'+k.TestersAM+'/',k.TestersAM+"_"+request.data['name']))
            te.delete()
        except FileNotFoundError:
            te.delete()
        except Exam.DoesNotExist:
            raise APIException('Δεν υπάρχει εξέταση με αυτό το όνομα')

        return Response('Done')


    @action(detail=False, methods=['get'], permission_classes=[AllowAny])
    def rmzip(self, request):
        try:
            os.remove(os.path.join(settings.MEDIA_ROOT, 'TestersCodes.zip'))
            os.remove(settings.MEDIA_ROOT+"/Results.txt")
        except FileNotFoundError:
            return Response('Οι συγκεκριμένοι πηγαίοι κώδικες των εξεταζόμενων δεν υπάρχουν')
        return Response('Done')

#0------------------------------------------------------------------------------------------------------#0------------------------------------------------------------------------------------------------------#0------------------------------------------------------------------------------------------------------




class ExamInstanceViewSet(viewsets.ModelViewSet):

    queryset = ExamInstance.objects.all()
    serializer_class = ExamInstanceSerializer


    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def savedcode(self , request):
        try:
            thecode = AM.objects.get(examtoken=request.data['temptoken1'])
            exam = Exam.objects.get(exam_name=request.data['exam'])
            inst = ExamInstance.objects.get(exam_type=exam.pk, student=thecode.pk)
            if not os.path.exists(os.path.join(settings.MEDIA_ROOT, 'TestersCodes/'+thecode.TestersAM+'/',(thecode.TestersAM+"_"+request.data['exam']+".cpp"))):
                inst.AmCode.save(thecode.TestersAM+"_"+request.data['exam']+".cpp", ContentFile('Welcome'))
            try:
                inst.AmCode.open()
                data = inst.AmCode.read()
                inst.AmCode.close()
            except ValueError:
                inst.AmCode.save(thecode.TestersAM+"_"+request.data['exam']+".cpp", ContentFile('Welcome'))
                inst.AmCode.open()
                data = inst.AmCode.read()
                inst.AmCode.close()
        except AM.DoesNotExist:
            raise APIException('Δεν βρέθηκε') 

        return Response( { 'data': data, 'type': exam.exam_type } )


    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def getTest(self , request):
        try: 
            _test = AM.objects.get(examtoken=request.data['temptoken1'])
            exam = Exam.objects.get(exam_name=request.data['exam'])
        except AM.DoesNotExist:
            raise APIException('Δεν υπάρχει το συγκεκριμένο ΑΜ')

        return Response( ExamSerializer(exam).data )


    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def code(self , request):
        try:
            thecode = AM.objects.get(examtoken=request.data['temptoken'])
            exam = Exam.objects.get(exam_name=request.data['exam'])
            inst = ExamInstance.objects.get(student=thecode.pk, exam_type=exam.pk)
            if os.path.exists(os.path.join(settings.MEDIA_ROOT, 'TestersCodes/'+thecode.TestersAM+'/',(thecode.TestersAM+"_"+request.data['exam']+".cpp"))):
                os.remove(os.path.join(settings.MEDIA_ROOT, 'TestersCodes/'+thecode.TestersAM+'/', (thecode.TestersAM+"_"+request.data['exam']+".cpp")))
            inst.AmCode.save(thecode.TestersAM+"_"+request.data['exam']+".cpp", ContentFile(request.data['code']))
        except AM.DoesNotExist:
            raise APIException('Κάτι πήγε στραβά')  

        return Response ( { True  } )


    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def compile(self , request):
        try:
            thecode = AM.objects.get(examtoken=request.data['temptoken'])
            inputs = Exam.objects.get(exam_name=request.data['exam'])
            inst = ExamInstance.objects.get(exam_type=inputs.pk, student=thecode.pk)
            #print(inputs.io_pairs)
            _dir= settings.MEDIA_ROOT+'/Tests/'
            _dir = os.path.join(_dir, thecode.TestersAM)
            if not os.path.exists(_dir):
                os.makedirs(_dir)
            cmd = os.path.join(settings.MEDIA_ROOT, 'TestersCodes/'+thecode.TestersAM+'/', (thecode.TestersAM+"_"+request.data['exam']+".cpp"))
            out = os.path.join(settings.MEDIA_ROOT, 'Tests/'+thecode.TestersAM+'/', thecode.TestersAM+"_"+request.data['exam'])
            if os.path.exists(out):
                os.remove(os.path.join(settings.MEDIA_ROOT, 'Tests/'+thecode.TestersAM+'/',thecode.TestersAM+"_"+request.data['exam']))
            #fout = inputs.the_output.save("Output_"+str(thecode2.exam.id), ContentFile(''))
            #outfi = os.path.join(settings.MEDIA_ROOT, 'Outputs/',("Output_"+str(thecode2.exam.id)))
            #st = os.stat(settings.MEDIA_ROOT+'/Outputs/'+'Output_'+str(thecode2.exam.id))
            #os.chmod(settings.MEDIA_ROOT+'/Outputs/'+'Output_'+str(thecode2.exam.id), st.st_mode | stat.S_IEXEC)
            #with open(outfi, "w+") as f:
            #for x in range(len(inputs.io_pairs)):
                #if inputs.io_pairs[x]['input']:
                #    with tempfile.TemporaryFile(mode = "r+b") as f:
                #        for i in range(len(inputs.io_pairs[x]['input'])):
                #            dat = str((inputs.io_pairs[x]['input'][i]))
                #            dat = bytes(dat, 'UTF-8')
                #            f.write(dat)
                #            f.seek(0)
                #            f.flush()

#----------------------------------Oloklhro kwdika-----------------------------------------


            if (inputs.exam_type=="exam-2" or inputs.exam_type=="exam-0"):
                temp = subprocess.Popen(["g++", "-o", out, cmd], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False, cwd=os.path.join(settings.MEDIA_ROOT, 'Tests/'+thecode.TestersAM+'/'))
                try:
                    stdout, stderr = temp.communicate(timeout=2)
                    if os.path.exists(out):
                        try:
                            outp = subprocess.check_output(["./"+thecode.TestersAM+"_"+request.data['exam']], shell=False, cwd=os.path.join(settings.MEDIA_ROOT, 'Tests/'+thecode.TestersAM+'/'), timeout=2)
                            outp = outp.decode("utf-8")
                        except:
                            raise APIException('Ο κώδικας που εκτελείτε δεν τελείωσε ποτέ. Μπορεί να υπάρχει ατέρμον βρόχος στον κώδικα ή επιστρέφεις κατι λανθασμένο!')
                except subprocess.TimeoutExpired:
                    raise APIException('Ο κώδικας που εκτελείτε δεν τελείωσε ποτέ. Μπορεί να υπάρχει ατέρμον βρόχος στον κώδικα')


                if stderr.decode("utf-8")!="":
                    return Response ( { 'result' : stderr.decode("utf-8").replace(settings.MEDIA_ROOT+'/TestersCodes/'+thecode.TestersAM+'/'+thecode.TestersAM+"_"+request.data['exam']+'.cpp:','').split(sep='\n') } ) 
            #os.rename(settings.MEDIA_ROOT+'/Outputs/'+'a.out',settings.MEDIA_ROOT+'/Outputs/'+thecode2.TestersAM)

                try:
                    tmp = subprocess.call(["./"+thecode.TestersAM+"_"+request.data['exam']], shell=False, stdout=subprocess.PIPE, cwd=os.path.join(settings.MEDIA_ROOT, 'Tests/'+thecode.TestersAM+'/'), timeout=2)
                except subprocess.TimeoutExpired:
                    raise APIException('Ο κώδικας που εκτελείτε δεν τελείωσε ποτέ. Μπορεί να υπάρχει ατέρμον βρόχος στον κώδικα')


#-------------------------------Meros tou kwdika---------------------------------------------


            if (inputs.exam_type=="exam-1"):
                examcode = os.path.join(settings.MEDIA_ROOT, 'TestsCodein/', request.data['exam']+".cpp")
                tpath = settings.MEDIA_ROOT+'/CodeTemp/'
                try:
                    temp = open(tpath+thecode.TestersAM+".cpp", "w+")
                    with open(cmd,"r+") as f:
                        temp.write(f.read())
                        with open(examcode,"r+") as fi:
                            temp.write(fi.read())
                            temp.seek(0)
                    #print(temp.read())
                    temppipe = subprocess.Popen(["g++", "-o", out, tpath+thecode.TestersAM+".cpp" ], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False, cwd=os.path.join(settings.MEDIA_ROOT, 'Tests/'+thecode.TestersAM+'/'))
                    try:
                        stdout, stderr = temppipe.communicate(timeout=2)
                        if os.path.exists(out):
                            try:
                                outp = subprocess.check_output(["./"+thecode.TestersAM+"_"+request.data['exam']], shell=False, cwd=os.path.join(settings.MEDIA_ROOT, 'Tests/'+thecode.TestersAM+'/'), timeout=2)
                                outp = outp.decode("utf-8")
                            except:
                                raise APIException('Ο κώδικας που εκτελείτε δεν τελείωσε ποτέ. Μπορεί να υπάρχει ατέρμον βρόχος στον κώδικα ή επιστρέφεις κατι λανθασμένο!')
                    except subprocess.TimeoutExpired:
                        raise APIException('Ο κώδικας που εκτελείτε δεν τελείωσε ποτέ. Μπορεί να υπάρχει ατέρμον βρόχος στον κώδικα')


                    if stderr.decode("utf-8")!="":
                        return Response ( { 'result' : stderr.decode("utf-8").replace(settings.MEDIA_ROOT+'/CodeTemp/'+thecode.TestersAM+'.cpp:','').split(sep='\n') } ) 
            #os.rename(settings.MEDIA_ROOT+'/Outputs/'+'a.out',settings.MEDIA_ROOT+'/Outputs/'+thecode2.TestersAM)

                    try:
                        tempcall = subprocess.call(["./"+thecode.TestersAM+"_"+request.data['exam']], shell=False, stdout=subprocess.PIPE, cwd=os.path.join(settings.MEDIA_ROOT, 'Tests/'+thecode.TestersAM+'/'), timeout=2)
                    except subprocess.TimeoutExpired:
                        raise APIException('Ο κώδικας που εκτελείτε δεν τελείωσε ποτέ. Μπορεί να υπάρχει ατέρμον βρόχος στον κώδικα')
                except FileNotFoundError:
                    raise APIException('Kάτι πήγε στραβά προσπάθησε πάλι!')
                finally:
                    os.remove(tpath+thecode.TestersAM+".cpp")
        except FileNotFoundError:
            raise APIException('Kάτι πήγε στραβά προσπάθησε πάλι!')
        except AM.DoesNotExist:
            raise APIException('Κάτι πήγε στραβά προσπάθησε πάλι!')

        return Response ( { 'output' : outp } )



#-------------------------------------Polaplis epilogis----------------------------------------------


            #if (inputs.exam_type=="exam-3"):


    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def checkcode(self , request):
        try:
            thecode = AM.objects.get(examtoken=request.data['temptoken'])
            inputs = Exam.objects.get(exam_name=request.data['exam'])
            inst = ExamInstance.objects.get(exam_type=inputs.pk, student=thecode.pk)
            #print(inputs.io_pairs)
            _dir= settings.MEDIA_ROOT+'/Tests/'
            _dir = os.path.join(_dir, thecode.TestersAM)
            cmd = os.path.join(settings.MEDIA_ROOT, 'TestersCodes/'+thecode.TestersAM+'/', (thecode.TestersAM+"_"+request.data['exam']+".cpp"))
            out = os.path.join(settings.MEDIA_ROOT, 'Tests/'+thecode.TestersAM+'/',thecode.TestersAM+"_"+request.data['exam'])
            if os.path.exists(out): 
                os.remove(os.path.join(settings.MEDIA_ROOT, 'Tests/'+thecode.TestersAM+'/',thecode.TestersAM+"_"+request.data['exam']))


#----------------------------------Oloklhro kwdika-----------------------------------------

#--------------------------------------Diorthwse ta lathoi------------------------------------


            if (inputs.exam_type=="exam-2" or inputs.exam_type=="exam-0"):
                temp = subprocess.Popen(["g++", "-o", out , cmd], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False, cwd=os.path.join(settings.MEDIA_ROOT, 'Tests/'))
                try:
                    stdout, stderr = temp.communicate(timeout=2)
                    if os.path.exists(out):
                        try:
                            outp = subprocess.check_output(["./"+thecode.TestersAM+"_"+request.data['exam']], shell=False, cwd=os.path.join(settings.MEDIA_ROOT, 'Tests/'+thecode.TestersAM+'/'),timeout=2)
                            outp = outp.decode("utf-8")
                        except: 
                            raise APIException('Τα αποτελέσματα εξόδου που προκύπτουν από τον κώδικά σου είναι λανθασμένα. Είσαι σίγουρος οτι θέλεις να υποβάλεις τον κώδικά σου?')
                except subprocess.TimeoutExpired:
                    raise APIException('Ο κώδικας που εκτελείτε δεν τελείωσε ποτέ. Μπορεί να υπάρχει ατέρμον βρόχος στον κώδικα')
                try:
                    tmp = subprocess.call(["./"+thecode.TestersAM+"_"+request.data['exam']], stdout=subprocess.PIPE, cwd=os.path.join(settings.MEDIA_ROOT, 'Tests/'+thecode.TestersAM+'/'), timeout=2, shell=False)
                    #print(inputs.io_pairs['output'])
                    if inputs.exam_type=="exam-2":
                        oout = "\n".join(inputs.io_pairs[0]['output'].splitlines())
                    else:
                        oout = "\n".join(inputs.io_pairs['output'].splitlines())
                    #print(cout)
                    #print(outp)
                    if oout==outp:
                        inst.passed = True
                        inst.save()
                    else:
                        inst.passed = False
                        inst.save()
                        raise APIException('Τα αποτελέσματα εξόδου που προκύπτουν από τον κώδικά σου είναι λανθασμένα. Είσαι σίγουρος οτι θέλεις να υποβάλεις τον κώδικά σου?')
                except subprocess.TimeoutExpired:
                    raise APIException('Ο κώδικας που εκτελείτε δεν τελείωσε ποτέ. Μπορεί να υπάρχει ατέρμον βρόχος στον κώδικα')


#-------------------------------------Polaplis epilogis----------------------------------------------


            if (inputs.exam_type=="exam-3"):
                with open(cmd, "r+") as f:
                    rea = f.readlines()
                    #out = rea.seek(0)
                if rea[-1]==inputs.io_pairs['output']:
                    inst.passed = True
                    inst.save()
                else:
                    inst.passed = False
                    inst.save()



#-------------------------------Meros tou kwdika---------------------------------------------


            if (inputs.exam_type=="exam-1"):
                examcode = os.path.join(settings.MEDIA_ROOT, 'TestsCodein/', request.data['exam']+".cpp")
                tpath = settings.MEDIA_ROOT+'/CodeTemp/'
                try:
                    temp = open(tpath+thecode.TestersAM+".cpp", "w+")
                    with open(cmd,"r+") as f:
                        temp.write(f.read())
                        with open(examcode,"r+") as fi:
                            temp.write(fi.read())
                            temp.seek(0)
                    #print(temp.read())
                    temppipe = subprocess.Popen(["g++", "-o", out , tpath+thecode.TestersAM+".cpp" ], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False, cwd=os.path.join(settings.MEDIA_ROOT, 'Tests/'+thecode.TestersAM+'/'))
                    try:
                        stdout, stderr = temppipe.communicate(timeout=2)
                        if os.path.exists(out):
                            try:
                                outp = subprocess.check_output(["./"+thecode.TestersAM+"_"+request.data['exam']], shell=False, cwd=os.path.join(settings.MEDIA_ROOT, 'Tests/'+thecode.TestersAM+'/'),timeout=2)
                                outp = outp.decode("utf-8")
                            except:
                                raise APIException('Ο κώδικας που εκτελείτε δεν τελείωσε ποτέ. Μπορεί να υπάρχει ατέρμον βρόχος στον κώδικα ή επιστρέφεις κατι λανθασμένο! Είσαι σίγουρος οτι θέλεις να υποβάλεις τον κώδικά σου?')
                    except subprocess.TimeoutExpired:
                        raise APIException('Ο κώδικας που εκτελείτε δεν τελείωσε ποτέ. Μπορεί να υπάρχει ατέρμον βρόχος στον κώδικα. Είσαι σίγουρος οτι θέλεις να υποβάλεις τον κώδικά σου?')
                    try:
                        tmpcall = subprocess.call(["./"+thecode.TestersAM+"_"+request.data['exam']], stdout=subprocess.PIPE, cwd=os.path.join(settings.MEDIA_ROOT, 'Tests/'+thecode.TestersAM+'/'), timeout=2, shell=False)
                            #print(repr(outp))
                        oout = "\n".join(inputs.io_pairs['output'].splitlines())
                        #print(repr(oout))
                        if oout==outp:
                            inst.passed = True
                            inst.save()
                        else:
                            inst.passed = False
                            inst.save()
                            raise APIException('Τα αποτελέσματα εξόδου που προκύπτουν από τον κώδικά σου είναι λανθασμένα. Είσαι σίγουρος οτι θέλεις να υποβάλεις τον κώδικά σου?')
                    except subprocess.TimeoutExpired:
                        raise APIException('Ο κώδικας που εκτελείτε δεν τελείωσε ποτέ. Μπορεί να υπάρχει ατέρμον βρόχος στον κώδικα. Είσαι σίγουρος οτι θέλεις να υποβάλεις τον κώδικά σου?')


                except FileNotFoundError:
                    raise APIException('Τα αποτελέσματα εξόδου που προκύπτουν από τον κώδικά σου είναι λανθασμένα. Είσαι σίγουρος οτι θέλεις να υποβάλεις τον κώδικά σου?')
                finally:
                    os.remove(tpath+thecode.TestersAM+".cpp")




        except FileNotFoundError:
            raise APIException('Kάτι πήγε στραβά. Είσαι σίγουρος οτι θέλεις να υποβάλεις τον κώδικά σου?')
        except AM.DoesNotExist:
            raise APIException('Κάτι πήγε στραβά προσπάθησε πάλι!')  

        return Response ( { 'pass' : inst.passed } )



    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def getCode(self , request):
        try:
            thecode = AM.objects.get(examtoken=request.data['temptoken1'])
            exam = Exam.objects.get(exam_name=request.data['exam'])
            inst = ExamInstance.objects.get(exam_type=exam.pk, student=thecode.pk)
            inst.AmCode.open()
            data = inst.AmCode.read()
            inst.AmCode.close()
        except AM.DoesNotExist:
            raise APIException('Doesnt Exist') 

        return Response( { data } )

    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def thetime(self , request):
        try:
            tester = AM.objects.get(examtoken=request.data['temptoken1'])
            exam = Exam.objects.get(exam_name=request.data['exam'])
            inst = ExamInstance.objects.get(exam_type=exam.pk, student=tester.pk)
            if not inst.timer:
                if exam.exam_type=='exam-1':
                    inst.timer = timezone.localtime() + datetime.timedelta(hours=2)
                if exam.exam_type=='exam-2':
                    inst.timer = timezone.localtime() + datetime.timedelta(hours=2)
                if exam.exam_type=='exam-0':
                    inst.timer = timezone.localtime() + datetime.timedelta(hours=0.5)
                if exam.exam_type=='exam-3':
                    inst.timer = timezone.localtime() + datetime.timedelta(hours=0.15)
                inst.save()
        except AM.DoesNotExist:
            raise APIException('Doesnt Exist') 

        return Response(  { 'endtime' : inst.timer, 'fin' : inst.finished } )

    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def _finish(self , request):
        try:
            allgood = AM.objects.get(examtoken=request.data['token'])
            exam = Exam.objects.get(exam_name=request.data['exam'])
            inst = ExamInstance.objects.get(exam_type=exam.pk, student=allgood.pk)
            inst.finished = True
            inst.save()
        except AM.DoesNotExist:
            raise APIException('Κάτι πήγε πολύ στραβά') 
        except Exam.DoesNotExist:
            raise APIException('Προσπάθησε πάλι')

        return Response( 'Its Done' )

    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def _pass(self,request):
        try:
            allgood = AM.objects.get(examtoken=request.data['token'])
            exam = Exam.objects.get(exam_name=request.data['exam'])
            inst = ExamInstance.objects.get(exam_type=exam.pk, student=allgood.pk)
        except AM.DoesNotExist:
            raise APIException('Προσπάθησε πάλι')
        except Exam.DoesNotExist:
            raise APIException('Προσπάθησε πάλι')


        return Response( { 'pass': inst.passed } )


    @action(detail=False, methods=['post'], permission_classes=[AllowAny])
    def jwtcheck(self , request):
        user = authenticate(token=request.data['token'])
        if user is not None:
            return Response( True )
        else:
            raise APIException('Ο προσωρινός κωδικός σας έληξε. Συνδεθείτε πάλι!')


