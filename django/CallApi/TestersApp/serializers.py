from rest_framework import serializers
from TestersApp.models import AM, Exam, ExamInstance
from rest_framework.exceptions import APIException
from drf_writable_nested.serializers import WritableNestedModelSerializer

class AMSerializer(serializers.ModelSerializer):

    class Meta:
        model = AM
        fields = "__all__"

class ExamSerializer(WritableNestedModelSerializer):

    ams = AMSerializer(required=False, many=True)
    
    class Meta:
        model = Exam
        fields = (
                'id',
                'exam_name',
                'exam_type',
                'the_tests',
                'the_input',
                'test_codein',
                'io_pairs',
                'finish',
                'date',
                'showexam',
                'ams',
                )

    def to_internal_value(self, data):
        theams = data['ams']
        existing_ams = []
        to_remove = []
        if (data['exam_name']==""):
            raise APIException('Ξέχασες το Όνομα της εξέτασης!')
        try:
            data['exam_type']
        except KeyError:
            raise APIException('Επέλεξε τύπο εξέτασης')
        if (data['ams']==[]):
            raise APIException('Ξεχάσατε να εισάγετε εξεταζόμενους')
        if not (data['the_tests']) or (data['the_input']==[]):
             raise APIException('Κάτι ξέχασες να συμπληρώσεις Ή Το περιεχόμενο του αρχείου που έχει εισαχθεί είναι λανθασμένο!')
        try:
            Exam.objects.get(exam_name=data['exam_name'])
            raise APIException('Το συγκεκριμένο Όνομα εξέτασης: %s ήδη υπάρχει' %data['exam_name'])
        except Exam.DoesNotExist:
            for am in theams:
                try:
                    existing_ams.append(AM.objects.get(TestersAM=am['TestersAM']))
                    to_remove.append(am)
                except AM.DoesNotExist:
                    continue 

        for am in to_remove:
            data['ams'].remove(am)
        
        data['ems'] = existing_ams
        data.pop('exam_type')
        data.pop('the_tests')
        data.pop('the_input')
        #print(data)
        return data #super().to_internal_value(data)

    def create(self, validated_data):
        #print(validated_data)
        data = validated_data.pop('ams')
        #print(data)
        data_ems = validated_data.pop('ems')
        exam = Exam.objects.create(**validated_data)
        for am in data: 
            AM.objects.create(**am)
            data_ems.append(AM.objects.get(TestersAM=am['TestersAM']))
        #print(data_ems)
        for am in data_ems:
            ExamInstance.objects.create(exam_type=exam, student=am)

        return exam


class ExamInstanceSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExamInstance
        fields = "__all__"

       # def create(self, validated_data):
       # data = validated_data
       # print(data)
       # aminstance = AMInstance.objects.create(**data)
       # TheExam_data = validated_data.pop('exam_name')
       # Tester_data = validated_data.pop('tester_am')
       # del TheExam_data['aminstance']
       # del Tester_data['aminstance']
       # Exam.objects.create(**TheExam_data, aminstance=aminstance)
       # AM.objects.create(**Tester_data, aminstance=aminstance)
       # return aminstance

