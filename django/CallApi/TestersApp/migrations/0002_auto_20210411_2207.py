# Generated by Django 3.1.3 on 2021-04-11 19:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('TestersApp', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aminstance',
            name='exam_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='TestersApp.exam'),
        ),
        migrations.AlterField(
            model_name='aminstance',
            name='student',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='TestersApp.am'),
        ),
    ]
