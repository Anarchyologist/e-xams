# Generated by Django 3.1.3 on 2021-04-13 11:30

import TestersApp.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TestersApp', '0007_remove_exam_ams'),
    ]

    operations = [
        migrations.AlterField(
            model_name='examinstance',
            name='AmCode',
            field=models.FileField(blank=True, null=True, upload_to=TestersApp.models.generate_filename),
        ),
    ]
