import uuid
from django.db import models
from datetime import datetime
# Create your models here.

class AM(models.Model):
    TestersAM = models.CharField(max_length=5, null=True, unique=True)
    email = models.EmailField(max_length=254, blank=True)
    unique_id = models.CharField(max_length=100, default=uuid.uuid4, editable=False, unique=True)
    examtoken = models.CharField(max_length=50, default=uuid.uuid4, editable=False, unique=False)


class Exam(models.Model):
    exam_name = models.CharField(max_length=100, null=True)
    exam_type = models.CharField(max_length=100, null=True, blank=True)
    the_tests = models.TextField(null=True, blank=True)
    the_input = models.FileField(null=True, blank=True, upload_to='Inputs/')
    test_codein = models.FileField(null=True, blank=True, upload_to='TestsCodein/')
    io_pairs = models.JSONField(null=True , blank=True)
    finish = models.BooleanField(null=True)
    date = models.DateTimeField(null=True, blank=True)
    showexam = models.BooleanField(default=False)


def generate_filename(self, filename):
    url = 'TestersCodes/%s/%s' %(filename[:filename.find("_")], filename)
    return url

class ExamInstance(models.Model):
    exam_type = models.ForeignKey(Exam, on_delete=models.SET_NULL, null=True, blank=True)
    student = models.ForeignKey(AM , on_delete=models.CASCADE, null=True, blank=True)
    AmCode = models.FileField(null=True, blank=True,upload_to=generate_filename)
    timer = models.DateTimeField(null=True, blank=True)
    finished = models.BooleanField(default=False)
    passed = models.BooleanField(default=False)


