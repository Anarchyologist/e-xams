from django.apps import AppConfig


class TestersappConfig(AppConfig):
    name = 'TestersApp'
