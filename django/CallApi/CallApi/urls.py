from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import refresh_jwt_token
from rest_framework_jwt.views import verify_jwt_token

urlpatterns = [
    path('admin/', admin.site.urls),
    path('am/api-token-auth/',obtain_jwt_token),
    path('am/api-token-refresh/', refresh_jwt_token),
    path('am/api-token-verify/', verify_jwt_token),
    path('am/', include('TestersApp.urls')),
]
