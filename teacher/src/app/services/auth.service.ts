import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable , of } from 'rxjs';
import { map , tap, catchError } from 'rxjs/operators';
import { Router } from "@angular/router";
import decode from 'jwt-decode';


@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private  _checkUrl = "/am/api-token-verify/";

    public getToken(): string {
        return localStorage.getItem('accessToken');
    }


    constructor(
        private http:HttpClient,
        private router:Router
    ) {}

    public isAuthenticated(): Observable<boolean> {
        const token = this.getToken();
        if(!token) {
            return of(false);
        }
        return this.http.post<any>(this._checkUrl,({ 'token' : token }))
        .pipe(
            map( res => {
                if (res.token == token) {
                    return true;
                } else return false;
            }),
            catchError(err => {
                return of(false);
            }),
        );
    }
}
