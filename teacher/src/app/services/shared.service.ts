import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable , of, throwError } from 'rxjs';
import { map , tap, catchError } from 'rxjs/operators';
import { Router } from "@angular/router";
import decode from 'jwt-decode';

@Injectable({
    providedIn: 'root'
})
export class SharedService {

    amcount=0;
    examcount='';

    private  _loginUrl = "/am/api-token-auth/";
    private  _sendmailUrl = "/am/sendmail/";
    private  _insertamUrl = "/am/exam/";
    private  _inserttestUrl = "/am/exam/inserttest/";
    private  _gettestUrl = "/am/exam/gettest/";    
    private  _deleteallUrl = "/am/exam/deleteall/";
    private  _deleteexamUrl = "/am/exam/deleteExam/";
    private  _deleteselUrl = "/am/exam/deletesel/";
    private  _getCodeUrl = "/am/getCode/";
    private  _getResuUrl = "/am/getResu/";
    private _rmzipUrl = "/am/exam/rmzip/"
    private _showselUrl = "/am/exam/showsel/"


    constructor(
        private http:HttpClient,
        private router:Router
    ) { }


    teacherlogin(Username: string, Password: string){
        this.http.post<any>(this._loginUrl, { 'username': Username, 'password' : Password } )
        .pipe(
            tap( res => {
                localStorage.setItem ('accessToken', res.token);
                this.router.navigate(['/home']);
            }),
            catchError(err => {
                //console.log(err);
                return of('Something is Missing');
            }),
        ).subscribe();
    }

    InsertExam(ams: Array<any>, examid: string, thetest: string, input: any, type: any): Observable<any>{
        //console.log(ams);
        //console.log(examid);
        return this.http.post<any>(this._insertamUrl, { 'exam_name': examid, 'exam_type': type, 'the_tests': thetest, 'the_input': input, 'ams': ams });
        /*.pipe(
          catchError(err => {
        //console.log(err);
        return of(err);
        }),
        ).subscribe();
         */
    }

    InsertExamInfo(ams: Array<any>, email: Array<any>, thetest: string, examid: string, input: any, type: any): Observable<any>{
        //console.log(input);
        return this.http.post<any>(this._inserttestUrl, { 'ams':ams, 'email': email, 'thetest': thetest, 'examid': examid, 'input': input, 'type': type } );
    }   

    GetTests(): Observable<any>{
        return this.http.get<any>(this._gettestUrl);
    }

    ShowExam(name:string) {
        return this.http.post<any>(this._showselUrl, { 'name': name } );
    }

    DeleteAllexam(){
        return this.http.get<any>(this._deleteallUrl);
    }

    DeleteExam(){
        return this.http.get<any>(this._deleteexamUrl);
    }

    DeleteSel(name:string): Observable<any>{
        return this.http.post<any>(this._deleteselUrl, { 'name': name } );
    }

    SendMail() {
        return this.http.get<any>(this._sendmailUrl);
    }

    getCode() {
        return this.http.get<any>(this._getCodeUrl);
    }

    getResu() {
        return this.http.get<any>(this._getResuUrl);
    }

    rmzip() {
        return this.http.get<any>(this._rmzipUrl);
    }

}
