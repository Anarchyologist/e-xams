import { Component, OnInit, ViewChild, AfterViewChecked, ViewContainerRef} from '@angular/core';
import { Router, RouterModule } from "@angular/router";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { SharedService } from '../services/shared.service';
import { MatSelect } from '@angular/material/select';
import { MatOption } from '@angular/material/core';
import { MatCheckbox } from '@angular/material/checkbox';
import { Observable , of } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HeaderComponent} from './header/header.component';



@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    the_tests: Array<any>;
    show_side = false;
    checked:boolean = false;
    finished = [];

    examtypes = [
        {value: 'exam-0', viewValue: 'Διόρθωσε τα λάθη'},
        {value: 'exam-1', viewValue: 'Γράψε Μέρος του κώδικα'},
        {value: 'exam-2', viewValue: 'Γράψε ολόκληρο κώδικα'},
        {value: 'exam-3', viewValue: 'Πολλαπλής Επιλογής'},
    ]

    constructor(
        private http:HttpClient,
        private shared: SharedService,
        private router:Router,
        private snackBar: MatSnackBar
    ) { }

    ngOnInit(): void {

        this.shared.GetTests()
        .pipe(
            tap( res => {
                this.the_tests=res.test;
                const obj = JSON.stringify(this.the_tests);
                for (let fi in this.the_tests){
                    this.the_tests[fi].count = res.count[fi];
                    this.the_tests[fi].sel = false;
                    for (let i in this.examtypes){
                        if(this.examtypes[i]['value']==this.the_tests[fi].exam_type){
                            this.the_tests[fi].exam_type = this.examtypes[i]['viewValue'];
                        }
                    }
                    const obj = this.the_tests[fi];
                    //console.log(this.the_tests[fi].selected);
                    if(obj.finish==true){
                        this.finished[fi]='Ναι';
                    }else{
                        this.finished[fi]='Όχι';
                    }
                }
            }),
            catchError(err => {
                return of(err);
            }),
        ).subscribe();
    }

    DeleteAll() {

        this.shared.DeleteAllexam()
        .pipe(
            map( res => {
                this.openSnackBar('Διαγράφτηκαν Επιτυχώς', '');
                //console.log(res);
            }),
            catchError(err => {
                return of(err);
            }),
        ).subscribe();

        for (let i in this.the_tests){
            if (this.the_tests[i].exam_name){
                this.the_tests[i].exam_name = false;
            }
        }

        this.shared.rmzip()
        .pipe(
            catchError(err => {
                return of(err);
            }),
        ).subscribe();
    }

    DeleteExam(){

        this.shared.DeleteExam()
        .pipe(
            map( res => {
                this.openSnackBar('Διαγράφτηκαν Επιτυχώς', '');
                //console.log(res);
            }),
            catchError(err => {
                return of(err);
            }),
        ).subscribe();

        for (let i in this.the_tests){
            if (this.the_tests[i].exam_name){
                this.the_tests[i].exam_name = false;
            }
        }

        this.shared.rmzip()
        .pipe(
            catchError(err => {
                return of(err);
            }),
        ).subscribe();
    }

    DeleteSelected() {
        for (let i in this.the_tests){
            if (this.the_tests[i].checked==true){
                this.shared.DeleteSel(this.the_tests[i].exam_name)
                .pipe(
                    map( res => {
                        this.openSnackBar('Διαγράφτηκε Επιτυχώς', '');
                    }),
                    catchError(err => {
                        return of(err);
                    }),
                ).subscribe();

                this.the_tests[i].exam_name = false;
            }else {
                this.openSnackBar('Επιλέξτε εξέταση!!!', '');
            }
        }
    }

    SendMail() {

        this.shared.SendMail()
        .pipe(
            map( res=> {
                this.openSnackBar('Στάλθηκαν Επιτυχώς', '');
                //console.log(res);
            }),
            catchError(err => {
                return of(err);
            }),
        ).subscribe();
    }

    GetTests() {

        this.shared.getCode()
        .pipe(
            map(res =>{
                this.http.get<any>("/media/TestersCodes.zip", {responseType: 'blob' as 'json'})
                .pipe(
                    map(res=> {
                        const blob: Blob = new Blob([res], { type: 'application/zip' });
                        const fileName = "Testers Codes";
                        const objectUrl: string = URL.createObjectURL(blob);
                        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
                        a.href = objectUrl;
                        a.download = fileName;
                        document.body.appendChild(a);
                        a.click();
                        document.body.removeChild(a);
                        URL.revokeObjectURL(objectUrl);
                    }),
                    catchError(err => {
                        return of(err);
                    }),
                ).subscribe();
            }),
            catchError(err => {
                return of(err);
            }),
        ).subscribe();
    }

    GetRes(){
        this.shared.getResu()
        .pipe(
            map(res => {
                this.http.get<any>("/media/Results.txt", {responseType: 'blob' as 'json'})
                .pipe(
                    map(res => {
                        const blob: Blob = new Blob([res], );
                        const fileName = "Γρήγορα Αποτελέσματα Εξετάσεων";
                        const objectUrl: string = URL.createObjectURL(blob);
                        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
                        a.href = objectUrl;
                        a.download = fileName;
                        document.body.appendChild(a);
                        a.click();
                        document.body.removeChild(a);
                        URL.revokeObjectURL(objectUrl);

                    }),
                    catchError(err => {
                        return of(err);
                    }),
                ).subscribe();
            }),
            catchError(err => {
                return of(err);
            }),
        ).subscribe();

    }

    Side(event) {
        this.show_side= !this.show_side;
    }

    onClick(event, index, test) {
        test.checked = !test.checked;
    }

    Selall() {
        for (let i in this.the_tests){
            this.the_tests[i].checked= !this.the_tests[i].checked;
        }
    }

    ShowSel() {
        for (let i in this.the_tests){
            if (this.the_tests[i].checked==true){
                this.shared.ShowExam(this.the_tests[i].exam_name)
                .pipe(
                    tap(res=>{}),
                        catchError(err=>{
                        return of(err);
                    }),
                ).subscribe();
                this.openSnackBar('Εμφανίστηκε Επιτυχώς', '');
            }else {
                this.openSnackBar('Επέλεξε εξέταση!!!', '');                
            }
        }
    }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            duration: 5000,
        });
    }


}
