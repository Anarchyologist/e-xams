import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, RouterModule } from "@angular/router";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { SharedService } from '../../services/shared.service';
import { Observable , of } from 'rxjs';
import { catchError, tap, map , mergeMap, switchMap} from 'rxjs/operators';
import { AngularEditorConfig } from '@kolkov/angular-editor';

import { MatOption } from '@angular/material/core';

import { FormsModule }  from '@angular/forms';
import * as QuillNamespace from 'quill';
let Quill: any = QuillNamespace;
import ImageResize from 'quill-image-resize-module';
import { ImageDrop } from 'quill-image-drop-module';
Quill.register('modules/imageDrop', ImageDrop);
Quill.register('modules/imageResize', ImageResize);

@Component({
    selector: 'app-addtest',
    templateUrl: './addtest.component.html',
    styleUrls: ['./addtest.component.css']
})
export class AddtestComponent implements OnInit {

    _input=[];
    theTest = '';
    exam_name = '';
    listofam: any;
    show = false;
    ams=[];
    email=[];
    tempam=[];
    tempe: any;
    sel_exam_type: any;
    quillModules: any;

    examtypes = [
        {value: 'exam-0', viewValue: 'Διόρθωσε τα λάθη'},
        {value: 'exam-1', viewValue: 'Γράψε Μέρος του κώδικα'},
        {value: 'exam-2', viewValue: 'Γράψε ολόκληρο κώδικα'},
        {value: 'exam-3', viewValue: 'Πολλαπλής Επιλογής'},
    ];

    examplein0 = [ `Παράδειγμα του περιεχομένου του αρχείου .txt:\n 10 234 1234\n23` ];
    examplein1 = [ `Παράδειγμα του περιεχομένου του αρχείου Output.txt:\n --------------------\nfrom file :0 0 0 0 1 0 1 1 0 1 1 0 1 0 1 1 0 0 0 0 1 1 0 0    k=1
462  freq =5
487  freq =11

   ............count=27
        ` ];
    examplein2 = [ `Παράδειγμα του περιεχομένου του αρχείου .cpp:\nΑπο τα Παρακάτω επιλέξτε το σωστό ανάλογα....... \n b x= 3\n β x=21\n a x=1\n...... <----- στην τελευταία γραμμή ακριβώς θα γραφεί η απάντηση` ];
    examplein3 = [ `Παράδειγμα του περιεχομένου του αρχείου .txt:\n b` ];
    examplein4 = [ `Παράδειγμα του περιεχομένου του αρχείου .cpp:
        int main()
{
    ifstream fg;
    fg.open("/webappz/pydev/sites/e-xams/django/CallApi/media/Inputs/(.........).txt"); <------- Εδώ (........) γράφετε το Όνομα της Εξέτασης
    if (!fg) {
        cout << "file does not open" << endl;
        exit(1);
    }
    processFile(fg); <------- Συνάρτηση που θα πρέπει να γράψει ο Εξεταζόμενος
    fg.close();
};` ];

    examplein =  [ `Παράδειγμα του περιεχομένου του αρχείου .txt:
        2104	487	0	0	0	0	1	0	1	1	0	1	1	0	1	0	1	1	0	0	0	0	1	1	0	0
1501	225	0	1	0	1	0	1	0	1	1	1	1	0	0	1	1	1	1	0	0	0	0	1	1	0
2505	20	0	0	0	1	0	1	0	0	1	0	1	1	0	1	0	0	0	0	0	1	1	0	1	0
1401	340	0	0	0	0	1	0	0	1	0	0	0	0	1	0	0	0	0	1	0	0	1	0	0	1
2010	29	0	0	0	0	0	0	1	1	1	1	0	1	0	0	0	0	0	1	0	1	1	0	1	0
2607	85	0	1	0	1	1	1	1	1	0	1	1	1	1	0	1	1	1	1	0	1	0	0	1	1
.....
        ` ];

    examplein5 = [ `Παράδειγμα του περιεχομένου του αρχείου .cpp:
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <string>
#define N 40
using namespace std;


int k=0, i, in_date, code, xx[24], c=0;
int temp[1000]={0};

int main()
{
	cout<<"10 234 1234"<<endl;
	cout<<"23";	
	return 0;
};
        ` ];

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private shared: SharedService,
    ) {}

    ngOnInit(): void {
        this.quillModules = {
            imageDrop: true,
            imageResize: {}
        };

    //console.log(this.examplein4);
    }
    
    onFileSelected(file: File[]) {
        if (this.sel_exam_type=='exam-2'){
            for (var i = 0; i < file.length; i++) {
                let fileReader = new FileReader();
                fileReader.onload = (event) => {
                    this._input = ( [ { 'output' : fileReader.result as string } ] ); 
                    //console.log(this._input);
                }
                fileReader.readAsText(file[i]);
                //console.log(file);
            }
        }

        if (this.sel_exam_type=='exam-1') {
            for (var i = 0; i < file.length; i++) {
                //console.log(this._input);
                let filname = file[i].name;
                let fileReader = new FileReader();
                fileReader.onload = (event) => {
                    let k = filname.substring(filname.lastIndexOf('.')+1);
                    if (k == "cpp") {
                        this._input[i] = ( [ { 'codein' : fileReader.result as string } ] );
                        //console.log(this._input[i]);
                    }else{
                        if (filname=="Output.txt"){
                            this._input[i] = ( [ { 'output' : fileReader.result as string } ] );
                        }else{
                            this._input[i] = ( [ { 'data' : fileReader.result as string } ] );
                        }
                    }
                }
                //console.log(this._input);
                fileReader.readAsText(file[i]);
                //console.log(this._input);
            }
        }

        if ((this.sel_exam_type=='exam-0')||(this.sel_exam_type=='exam-3')) {
            for (var i = 0; i < file.length; i++) {
                //console.log(this._input);
                let filname = file[i].name;
                let fileReader = new FileReader();
                fileReader.onload = (event) => {
                    let k = filname.substring(filname.lastIndexOf('.')+1);
                    if (k == "cpp") {
                        this._input[i] = ( [ { 'codein' : fileReader.result as string } ] );
                        //console.log(this._input[i]);
                    }else{
                        this._input[i] = ( [ { 'output' : fileReader.result as string } ] );
                    }
                }
                //console.log(this._input);
                fileReader.readAsText(file[i]);
                //console.log(this._input);
            }
        }
    }

    onValueChange(file: File[]) {
        let fileReader = new FileReader();
        fileReader.onload = (event) => {
            var lines = (fileReader.result as string).split('\r\n');
            lines = lines.map( x => x.trim());
            lines = lines.filter( x => /^\d+/.test(x));
            //console.log(lines1);
            for (let i = 0; i < lines.length; i++) {
                //console.log(this.ams);
                this.tempe = lines[i].match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
                this.email.push(this.tempe.join());
                this.tempe = lines[i].match(/\d+/).map(Number);
                this.ams.push({'TestersAM': this.tempe.join()});
                this.tempam.push(this.tempe.join());
            }
            //console.log(lines);
            this.ams = [ ...this.ams];
            this.tempam = [ ...this.tempam];
            //console.log(lines);
            //console.log(lines1);
            this.listofam = fileReader.result;
            this.show = true;
        };
        fileReader.readAsText(file[0]);
        //console.log(file);
    }

    onSubmit() {
        //tap( res => console.log(res)),
        this.shared.InsertExam(this.ams, this.exam_name, this.theTest, this._input, this.sel_exam_type).pipe(
            switchMap(()=> this.shared.InsertExamInfo(this.ams, this.email, this.theTest, this.exam_name, this._input, this.sel_exam_type )),
                tap( res => this.router.navigate(['/home'])),
                catchError(err => {
                //console.log(err);
                return of(err);
            }),
            catchError(err => {
                return of(err);
            }),
        ).subscribe();
    }
    

    show_side = false;
    
    Side(event) {
        this.show_side= !this.show_side;
    }

}

