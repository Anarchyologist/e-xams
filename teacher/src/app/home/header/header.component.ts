import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, RouterModule } from "@angular/router";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { SharedService } from '../../services/shared.service';
import { Observable , of } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css'],
    animations: [
        trigger('slideInOut', [
            state('in', style({
                transform: 'translate3d(0,0,0)',
            })),
            state('out', style({
                transform: 'translate3d(0,0, 0)',
            })),
            transition('in => out', animate('400ms ease-in-out')),
            transition('out => in', animate('400ms ease-in-out'))
        ]),
    ],
})
export class HeaderComponent implements OnInit {

    constructor(
        private shared: SharedService,
        private router:Router
    ) { }

    ngOnInit(): void {
    }
    
    @Output() newItemEvent = new EventEmitter<string>();

    sidebarState : string = 'out';

    toggleSidebar(){
        this.sidebarState = this.sidebarState === 'out' ? 'in' : 'out';
        this.newItemEvent.emit(this.sidebarState);
    };




}
