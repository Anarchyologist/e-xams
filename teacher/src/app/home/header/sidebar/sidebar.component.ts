import { Component, Input, OnInit ,Injectable, OnChanges} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable , of , timer } from 'rxjs';
import { map , tap, catchError } from 'rxjs/operators';
import { Router, RouterModule, ActivatedRoute, ParamMap } from "@angular/router";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { trigger, state, style, transition, animate } from '@angular/animations';
import {MatIconModule} from '@angular/material/icon'
import { SharedService } from '../../../services/shared.service';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnChanges {



    @Input() show: string;

    constructor(
        private shared: SharedService,
        private router:Router
    ) {}

    ngOnChanges(): void {}

    Logout() {
        localStorage.removeItem('accessToken');
        this.router.navigate(['login']);
    }
}
