import { Injectable } from "@angular/core";
import { AuthService } from '../services/auth.service';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse ,  HttpErrorResponse } from '@angular/common/http';
import { NavigationEnd, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of , throwError } from 'rxjs';
import { retry , map, catchError, mergeMap, tap } from 'rxjs/operators';
import { debug } from 'util';
import { ErrorService } from '../services/error.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    refreshing : boolean = false;

    constructor(
        private auth: AuthService,
        private router: Router,
        private http: HttpClient,
        private errorService: ErrorService,
    ) {}

    parseJwt (token) {

        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));

        return JSON.parse(jsonPayload);
    }

    intercept(
        request: HttpRequest<any>, 
        next: HttpHandler
    ): Observable<HttpEvent<any>>{

        if(request.headers.get('skip')) {
            return next.handle(request);
        }

        let token = this.auth.getToken();
        if( token ) {

            let exp = new Date(0);
            exp.setUTCSeconds(this.parseJwt(token).exp);
            const now = new Date( Date.now() );
            const diff = exp.getTime() - now.getTime() ;
            const minsLeft = Math.round(((diff % 86400000) % 3600000) / 60000);
            //console.log(minsLeft); 
            if(minsLeft > 0 && minsLeft < 15 && !this.refreshing && !request.headers.get('no_token_refresh')) { 
                this.refreshing = true;
                //Refresh the fucking token
                let httpHeaders = new HttpHeaders();
                httpHeaders.set('Content-Type', 'application/json');
                httpHeaders = httpHeaders.append('skip', 'true');
                this.http.post<any>('am/api-token-refresh/', { 'token' : token } , { headers: httpHeaders}).pipe().subscribe( res => { 
                    localStorage.setItem('accessToken', res.token);
                    this.refreshing = false;
                });
            }

            request  = request.clone({
                setHeaders: { Authorization: `Bearer ${this.auth.getToken()}` }
            });
        }
        return next.handle(request).pipe(
            //retry(1),
            tap(
                event => {
                    if (event instanceof HttpResponse) {
                    }
                },
                error => {
                    if(error.status == 400) {
                        let httpHeaders = new HttpHeaders();
                        httpHeaders.set('Content-Type', 'application/json');
                        httpHeaders = httpHeaders.append('skip', 'true');
                        this.http.post<any>('am/api-token-verify/', { 'token' : localStorage.getItem('accessToken')}, { headers: httpHeaders}).pipe().subscribe(
                            res => {
                            },
                            err => {
                                if( err.error.non_field_errors) {
                                    if(
                                        err.error.non_field_errors[0] ===  'Error decoding signature.' ||
                                        err.error.non_field_errors[0] ===  'Signature has expired.'
                                    ) {
                                        localStorage.removeItem('accessToken');
                                        this.router.navigate(['/login']);
                                    }
                                }
                                return err;
                            }
                        );
                    }
                }
            ),
            catchError((error:HttpErrorResponse) => {
                let data = {};
                if ( (error.error.username) || (error.error.password)) {
                    data = { 'detail': 'Κάτι ξέχασες'};
                }else if( error.error.non_field_errors) { 
                    if(error.error.non_field_errors[0] === 'Sigature has expired.' ||
                       error.error.non_field_errors[0] ===  'This field may not be null.'
                      ) {
                          data = { 'detail': 'Συνδέσου ξανά το κλειδί σου έληξε'};
                      }else if(error.error.non_field_errors[0] === 'Unable to log in with provided credentials.') {
                          data = { 'detail': 'Κάτι είναι λανθασμένο'};
                      }
                }else if(error.status == 413) {
                    data = { 'detail': 'The image you uploading is too large!'};
                }
                else{
                    data = error && error.error.non_field_errors && error.error && error.error.reason ? error.error.reason : error.error ||  error.error.non_field_errors;
                }
                this.errorService.openDialog(data);
                return throwError(error);

            })) as Observable<HttpEvent<any>>;
    }
}
