import {HTTP_INTERCEPTORS } from '@angular/common/http';
import {AuthInterceptor} from './authinterceptor';


export const httpInterceptProviders = [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }

];

