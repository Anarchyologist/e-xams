import { Component, OnInit } from '@angular/core';
import { SharedService } from '../services/shared.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';


@Component({
    selector: 'app-teacherlogin',
    templateUrl: './teacherlogin.component.html',
    styleUrls: ['./teacherlogin.component.css']
})
export class TeacherloginComponent implements OnInit {

    public Username: string;
    public Password: string;

    constructor(
        private shared: SharedService,
        private router: Router
    ) { }

    ngOnInit(): void {
              if (localStorage.getItem('accessToken')!=null)
                this.router.navigate(['/home']);
    }

    teacherlogin() {
        this.shared.teacherlogin(this.Username, this.Password);
    }
}

