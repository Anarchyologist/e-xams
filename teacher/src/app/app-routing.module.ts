import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { TeacherloginComponent } from './teacherlogin/teacherlogin.component';
import { HomeComponent } from './home/home.component';
import { AddtestComponent } from './home/addtest/addtest.component';
import { AuthGuard } from './services/auth-guard.service';

const routes: Routes = [

    { path: '',
        pathMatch: 'full',
        redirectTo: 'home',
    },
    { path: 'login',
        component: TeacherloginComponent,
    },
    { path: 'home',
        component: HomeComponent,
        canActivate: [AuthGuard],
    },
    { path: 'addexam',
        component: AddtestComponent,
        canActivate: [AuthGuard],
    },
    {path: '**', redirectTo: 'home', pathMatch: 'full'},
    {path: '', redirectTo: 'home', pathMatch: 'full'},
];


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
