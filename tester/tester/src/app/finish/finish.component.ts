import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from "@angular/router";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { ApicodeService } from '../services/apicode.service';
import { Observable , of } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

@Component({
    selector: 'app-finish',
    templateUrl: './finish.component.html',
    styleUrls: ['./finish.component.css']
})
export class FinishComponent implements OnInit {

    ispass:any;

    constructor(

        private apicode: ApicodeService,
        private router:Router  ) { }

        ngOnInit():Observable<any> {

            if (!localStorage.getItem('CurrentExam')) {
                this.router.navigate(['home']);
            }

            var k = this.apicode.pass();
            k.pipe(
                tap( res => {
                    if (res.pass==true){
                        this.ispass = 'Επιτυχή Αποτελέσματα';
                    }else{
                        this.ispass = 'Λανθασμένα Αποτελέσματα';
                    }
                    this.apicode.finished().pipe(
                        tap( res => { 
                        }),
                        catchError(err => {
                            return of(err);
                        }),
                    ).subscribe();
                }),
                catchError(err => {
                    return of(err);
                }),
            ).subscribe();
            return k;
        }

        Home() {
            localStorage.removeItem('CurrentExam');
        }

        Logout() {
            this.apicode.Logout()
            .pipe(
                map( res => {
                    localStorage.removeItem('CurrentExam');
                    localStorage.removeItem('UserToken');
                    this.router.navigate(['login']);
                    return res;
                }),
                catchError(err => {
                    return of(err);            
                }),
            ).subscribe();
        }
}

