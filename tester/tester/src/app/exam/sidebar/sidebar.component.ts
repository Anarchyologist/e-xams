import { Component, Input, OnInit ,Injectable} from '@angular/core';
import { ApicodeService } from '../../services/apicode.service';
import { HttpClient } from '@angular/common/http';
import { Observable , of , timer } from 'rxjs';
import { map , tap, catchError } from 'rxjs/operators';
import { Router, RouterModule, ActivatedRoute, ParamMap } from "@angular/router";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

    get_test: Array<string>;

    constructor(
        private apicode: ApicodeService,
        private router: Router,  
    ) { }

    ngOnInit(): void {
        this.apicode.getTest().pipe(
            tap( res => {
                //console.log(res);
                var doc = res.the_tests;
                //this.get_test = Array.of(doc.replace(/<[^>]+>/g, '\n'));
                this.get_test = doc;
                //console.log(this.get_test);
            }),
            catchError(err => {
                return err;
            }),
        ).subscribe();

    }

}
