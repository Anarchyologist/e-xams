import { Component, Input, OnInit ,Injectable} from '@angular/core';
import { ApicodeService } from '../services/apicode.service';
import { HttpClient } from '@angular/common/http';
import { Observable , of , timer } from 'rxjs';
import { map , tap, catchError } from 'rxjs/operators';
import { Router, RouterModule, ActivatedRoute, ParamMap } from "@angular/router";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
    selector: 'app-exam',
    templateUrl: './exam.component.html',
    styleUrls: ['./exam.component.css'],
    animations: [
        trigger('slideInOut', [
            state('in', style({
                transform: 'translate3d(0,0,0)'
            })),
            state('out', style({
                transform: 'translate3d(100%, 0, 0)'
            })),
            transition('in => out', animate('400ms ease-in-out')),
            transition('out => in', animate('400ms ease-in-out'))
        ]),
    ],
})
export class ExamComponent implements OnInit {

    cerror: Array<any>;  
    coutp: Array<any>;
    timeStop : Date;
    code: string = '';
    options : any = {
        lineNumbers: true,
        theme: 'material',
        mode: 'text/x-c++src',
        indent: true,
    };
    _now : Date;
    timeLeft : Observable<any>;
    examtype: string;

    constructor(
        private apicode: ApicodeService,
        private router: Router,
    ) { }

    ngOnInit(): void {
        this.apicode.getCode().pipe(
            tap( res => { 
                this.code = res.data;
                this.examtype = res.type;
                //console.log(this.examtype);
            }),
        ).subscribe();

        this.apicode.timerStart()
        .pipe(
            tap( res => { 
                if (res.fin==true){
                    this.router.navigate(['/finish']);
                }
                this.timeStop = new Date(res.endtime);
            }),
            catchError(err => {
                return of(err); 
            }),        
        ).subscribe();

        let source = timer(1000,1000);
        this.timeLeft = source.pipe(
            map( res =>  this.timeStop.getTime() - new  Date().getTime()),
                tap( res => { 
                if (res < 0){
                    this.router.navigate(['/finish']);
                }
            }
                   )
        );
        this.timeLeft.subscribe();
    }

    onSubmit() {
        this.apicode.onSubmit(this.code)
        .pipe(
            tap( res => {
                if (res=true){
                    this.apicode.compile(this.code)
                    .pipe(
                        tap( res => {
                            this.cerror = res.result;
                            //console.log(res.output);
                            this.coutp = res.output;
                            //console.log(res);
                            //console.log(this.cerror);
                        }),
                        catchError(err => {
                            //console.log('kati dn paei kala', err);
                            return of(err);
                        }),
                    ).subscribe();
                }}),
        ).subscribe();
    }

    Ready() {
        this.apicode.onSubmit(this.code)
        .pipe(
            tap( res => {
                if (res=true){
                    this.apicode.onReady(this.code);
                }}),
        ).subscribe();
    }

    sidebarState : string = 'out';

    toggleSidebar(){
        this.sidebarState = this.sidebarState === 'out' ? 'in' : 'out';
    };

}
