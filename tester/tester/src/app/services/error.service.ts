import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable , of } from 'rxjs';
import { catchError, tap, map , mergeMap} from 'rxjs/operators';
import { ErrorComponent } from '../error/error.component';

@Injectable()
export class ErrorService {

    public isDialogOpen: Boolean = false;

    constructor(
        public dialog: MatDialog
    ) { }

    openDialog(data): any {
        if (this.isDialogOpen) {
            return false;
        }else if(!data['detail']) {
            return false;
        }else{
            this.isDialogOpen = true;
        }
        
        const dialogRef = this.dialog.open(ErrorComponent, {
            panelClass: 'dialog',
            data: data
        });

        dialogRef.afterClosed().subscribe(result => {
            this.isDialogOpen = false;
        });
    }
}

