import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable , of } from 'rxjs';
import { map , tap, catchError, mergeMap, switchMap } from 'rxjs/operators';
import { Router } from "@angular/router";

@Injectable({
    providedIn: 'root'
})
export class ApicodeService {

    public theexam = "";

    private _codeUrl = "/am/instance/code/";
    private _savedcodeUrl = "/am/instance/savedcode/";
    private _logoutUrl = "/am/logout/";
    private _compileUrl = "/am/instance/compile/";
    private _timeUrl = "/am/instance/thetime/";
    private _checkcodeUrl = "/am/instance/checkcode/";
    private _finishUrl = "/am/instance/_finish/";
    private _passUrl = "/am/instance/_pass/";
    private _getTestUrl = "/am/instance/getTest/";
    private  _gettestUrl = "/am/gettestam/";

    constructor(
        private http:HttpClient,
        private router:Router
    ) { }

    Start(exam: string) : Observable<any> {
        localStorage.setItem('CurrentExam', exam);
        return this.http.post<any>(this._savedcodeUrl, {'temptoken1' : localStorage.getItem('UserToken'), 'exam' : exam});
    }

    GetTests(): Observable<any>{
        let exam = localStorage.getItem('CurrentExam');
        return this.http.post<any>(this._gettestUrl, {'temptoken' : localStorage.getItem('UserToken'), 'exam': exam});
    }

    onSubmit(code: string) : Observable<any> {
        let exam = localStorage.getItem('CurrentExam');
        return this.http.post<any>(this._codeUrl, {'temptoken' : localStorage.getItem('UserToken'), 'code' : code, 'exam' : exam});
    }

    onReady(code: string) {
        let exam = localStorage.getItem('CurrentExam');
        this.http.post<any>(this._checkcodeUrl, {'temptoken' : localStorage.getItem('UserToken'), 'code' : code, 'exam' : exam}).pipe(
            tap( res => {
                if (res.pass=true){
                    this.router.navigate(['/finish']);
                }}),
                catchError(err => {
                    return of(err);            
                }),
        ).subscribe();
    }

    compile(code: string) : Observable<any> {
        let exam = localStorage.getItem('CurrentExam');
        return this.http.post<any>(this._compileUrl, {'temptoken' : localStorage.getItem('UserToken'), 'code' : code, 'exam' : exam});
    }

    pass(){
        let token = localStorage.getItem('UserToken');
        let exam = localStorage.getItem('CurrentExam');
        return this.http.post<any>(this._passUrl,{ 'token' : token, 'exam': exam });
    }
    finished() {
        let token = localStorage.getItem('UserToken');
        let exam = localStorage.getItem('CurrentExam');
        return this.http.post<any>(this._finishUrl,{ 'token' : token, 'exam': exam });
    } 

    Logout(){
        let temptoken1 = localStorage.getItem('UserToken');
        return this.http.post<any>(this._logoutUrl, {'temptoken1' : temptoken1});
    }

    getCode(){
        let temptoken1 = localStorage.getItem('UserToken');
        let exam = localStorage.getItem('CurrentExam');
        return this.http.post<any>(this._savedcodeUrl, {'temptoken1' : temptoken1, 'exam': exam});
    }

    getTest() : Observable<any>{
        let temptoken1 = localStorage.getItem('UserToken');
        let exam = localStorage.getItem('CurrentExam');
        return this.http.post<any>(this._getTestUrl, {'temptoken1' : temptoken1, 'exam': exam});
    }

    timerStart(){
        let temptoken1 = localStorage.getItem('UserToken');
        let exam = localStorage.getItem('CurrentExam');
        return this.http.post<any>(this._timeUrl, {'temptoken1' : temptoken1, 'exam': exam});
    }


}

