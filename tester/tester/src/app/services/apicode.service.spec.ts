import { TestBed } from '@angular/core/testing';

import { ApicodeService } from './apicode.service';

describe('ApicodeService', () => {
  let service: ApicodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApicodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
