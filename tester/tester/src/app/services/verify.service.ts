import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable , of } from 'rxjs';
import { map , tap, catchError } from 'rxjs/operators';
import { Router } from "@angular/router";

@Injectable({
    providedIn: 'root'
})
export class VerifyService {

    private  _loginUrl = "/am/login/";
    private  _checkUrl = "/am/check/";

    constructor(
        private http:HttpClient,
        private router:Router
    ) { }

    public isAuthenticated(): Observable<boolean> {
        const token = localStorage.getItem('UserToken');
        if (token!=null){
            return this.http.post<any>(this._checkUrl, { 'token' : token })
            .pipe(
                map( res => {
                    if (res == true) {
                        return true;
                    }}),
                    catchError(err => {
                        return of(false);
                    }),
            );
        }
        if (token==null) {
            return of(false);
        }
    }

    loginT(AM: string, unique_id: string) {
        this.http.post<any>(this._loginUrl, { 'AM': AM , 'unique_id' : unique_id } )
        .pipe(
            tap( res => {
                localStorage.setItem('UserToken', res);
                return this.router.navigate(['/home']);
            }),
            catchError(err => {
                return of(err);
            }),
        ).subscribe();
    }


}

