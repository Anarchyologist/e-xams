import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {VerifyService} from './services/verify.service';
import {ApicodeService} from './services/apicode.service';
import { ErrorService } from './services/error.service';

import { httpInterceptProviders } from './http-interceptors';
import { AuthGuard } from './services/auth-guard.service';

import { CodeEditorModule } from '@ngstack/code-editor';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';



import { TesterloginComponent } from './testerlogin/testerlogin.component';
import { HomeComponent } from './home/home.component';
import { ExamComponent } from './exam/exam.component';
import { CodemirrorModule } from '@ctrl/ngx-codemirror';
import { SidebarComponent } from './exam/sidebar/sidebar.component';
import { FinishComponent } from './finish/finish.component';
import { ErrorComponent } from './error/error.component';

import { DatePipe } from '@angular/common';
import { LuxonDurationModule } from 'ng-luxon-duration';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};


@NgModule({
    declarations: [
        AppComponent,
        TesterloginComponent,
        HomeComponent,
        ExamComponent,
        SidebarComponent,
        FinishComponent,
        ErrorComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        CodemirrorModule,
        LuxonDurationModule,
        PerfectScrollbarModule,
        MatInputModule,
        MatFormFieldModule,
        MatCheckboxModule,
        MatDialogModule,
    ],
    providers: [
        VerifyService, 
        AuthGuard, 
        httpInterceptProviders,
        ApicodeService,
        ErrorService,
        DatePipe,
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }

    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
