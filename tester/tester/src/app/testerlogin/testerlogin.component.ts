import { Component, OnInit } from '@angular/core';
import { VerifyService } from '../services/verify.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';


@Component({
    selector: 'app-testerlogin',
    templateUrl: './testerlogin.component.html',
    styleUrls: ['./testerlogin.component.css']
})
export class TesterloginComponent implements OnInit {


    public AM: string;
    public unique_id: string;


    constructor(
        private verify: VerifyService,
        private router: Router
    ) { }

    ngOnInit(): void {
        if (localStorage.getItem('UserToken')!=null)
            this.router.navigate(['/home']);
    }

    loginT() {
        this.verify.loginT(this.AM, this.unique_id);
    }

    Teacher(){
        window.location.href = '/examiner/';
    }

}


