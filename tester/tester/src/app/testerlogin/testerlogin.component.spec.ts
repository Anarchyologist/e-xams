import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TesterloginComponent } from './testerlogin.component';

describe('TesterloginComponent', () => {
  let component: TesterloginComponent;
  let fixture: ComponentFixture<TesterloginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TesterloginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TesterloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
