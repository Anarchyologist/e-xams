import { Injectable } from "@angular/core";
import { VerifyService } from '../services/verify.service'; 
import { ErrorService } from '../services/error.service';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse ,  HttpErrorResponse } from '@angular/common/http';
import { NavigationEnd, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of , throwError } from 'rxjs';
import { retry , map, catchError, mergeMap, tap } from 'rxjs/operators';
import { debug } from 'util';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private verify: VerifyService,
                private router: Router,
                private http: HttpClient,
                private errorService: ErrorService,
               ) {}

               intercept(
                   request: HttpRequest<any>, 
                   next: HttpHandler 
               ):Observable<HttpEvent<any>> {

                   if(request.headers.get('skip')) {
                       return next.handle(request);
                   }
                   if ( localStorage.getItem('UserToken')) {

                   request  = request.clone({
                       setHeaders: { Authorization: `${localStorage.getItem('UserToken')}`  }
                   });
                   }
                   return next.handle(request).pipe(
                       //retry(1),
                       tap(
                           event => {
                               //console.log(event);
                               if (event instanceof HttpResponse) {
                               }
                           }),
                           catchError((error:HttpErrorResponse) => {
                               let data = {};
                               data = error && error.error && error.error.reason ? error.error.reason : error.error ;
                               //console.log(data);
                               //console.log(error);
                               this.errorService.openDialog(data);
                               return throwError(error);
                           })) as Observable<HttpEvent<any>>;
               }
}




