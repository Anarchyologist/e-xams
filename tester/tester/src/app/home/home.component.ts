import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from "@angular/router";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { ApicodeService } from '../services/apicode.service';
import { Observable , of, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { MatSelect } from '@angular/material/select';
import { MatOption } from '@angular/material/core';
import { MatCheckbox } from '@angular/material/checkbox';
import { ErrorService } from '../services/error.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    public temptoken1 = localStorage.getItem('UserToken');
    finished: any;
    selected = -1;
    the_tests: Array<any>;
    am: string;
    fin: Array<any>;

    examtypes = [
        {value: 'exam-0', viewValue: 'Διόρθωσε τα λάθη'},
        {value: 'exam-1', viewValue: 'Γράψε Μέρος του κώδικα'},
        {value: 'exam-2', viewValue: 'Γράψε ολόκληρο κώδικα'},
        {value: 'exam-3', viewValue: 'Πολλαπλής Επιλογής'},
    ]



    constructor(
        private apicode: ApicodeService,
        private router:Router,
        private errorService: ErrorService,
    ) { }

    ngOnInit(): void {

        this.apicode.GetTests()
        .pipe(
            tap( res => {
                this.am=res.am;
                this.fin=res.inst;
                this.the_tests=res.exam;
                for (let fi in this.the_tests){
                    for (let i in this.examtypes){
                        if(this.examtypes[i]['value']==this.the_tests[fi].exam_type){
                            this.the_tests[fi].exam_type = this.examtypes[i]['viewValue'];
                        }
                    }
                }
            }),
            catchError(err => {
                return of(err);
            }),
        ).subscribe();

    }

    Start(selected) {
        //console.log(selected);
        if (selected==-1) {
            try {
                throw new Error('Επέλεξε μια Εξέταση!');
            }
            catch (error) {
                //console.log(error)
                let data = {};
                data = { detail: 'Επέλεξε μια Εξέταση!' };
                this.errorService.openDialog(data);
                return throwError(error);
            }
        };
        //console.log(this.selected);
        //console.log(this.the_tests[selected].exam_name);
        this.apicode.Start(this.the_tests[selected].exam_name).pipe(
            tap( res => {
                this.router.navigate(['exam']);
                //console.log(res)
            }),
            catchError(err => {
                return of(err);
            }),
        ).subscribe();
    }

    Logout() {
        this.apicode.Logout()
        .pipe(
            map( res => {
                localStorage.removeItem('UserToken');
                this.router.navigate(['login']);
                return res;
            }),
            catchError(err => {
                return of(err);            
            }),
        ).subscribe();
    }

    //onClick(event, index, test) {
    //    test.checked = !test.checked;
    //}


}
