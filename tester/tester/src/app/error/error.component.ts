import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable , of } from 'rxjs';
import { Router } from "@angular/router";

@Component({
    selector: 'app-error',
    templateUrl: './error.component.html',
    styleUrls: ['./error.component.css']
})
export class ErrorComponent {

    constructor(
        private router:Router,
        @Inject(MAT_DIALOG_DATA)
        public data: any
    ) {}
    ngOninit() {
    }

    Yes() {
        this.router.navigate(['/finish']);
    }
}

